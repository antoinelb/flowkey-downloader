#!/bin/python3
import shutil
from getpass import getpass
from pathlib import Path
from typing import Any

import httpx
from PIL import Image, ImageChops
from reportlab.lib.utils import ImageReader
from reportlab.pdfgen import canvas
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.firefox.webelement import FirefoxWebElement
from selenium.webdriver.support.expected_conditions import (
    element_to_be_clickable,
    presence_of_all_elements_located,
    presence_of_element_located,
)
from selenium.webdriver.support.ui import WebDriverWait

timeout = 5

########
# main #
########


def main() -> None:
    print("Flowkey downloader")
    print("==================")

    browser = get_browser()
    browser = sign_in(browser)
    info = search(browser)
    info = download(browser, info)
    create_pdf(info)

    browser.close()
    log = Path("geckodriver.log")
    if log.exists():
        log.unlink()


def get_browser() -> WebDriver:
    load_print("Initializing browser...")
    options = Options()
    options.headless = True
    browser = webdriver.Firefox(options=options)
    browser.get("https://app.flowkey.com/login")
    print("".ljust(80), end="\r")
    return browser


def sign_in(browser: WebDriver) -> WebDriver:
    path = Path("~").expanduser() / ".cache" / "flowkey" / "credentials"
    if path.exists():
        with open(path) as f:
            _email = f.readline().strip()
            _password = f.readline().strip()
    else:
        _email, _password = None, None  # type: ignore

    if _email is None:
        email = input("Email address: ")
    else:
        email = input(f"Email address[{_email}]: ")
        if not email:
            email = _email
    if _password is None:
        password = getpass("Password: ")
    else:
        password = getpass("Password[***]: ")
        if not password:
            password = _password

    email_ = WebDriverWait(browser, timeout).until(
        presence_of_element_located(
            (
                By.CSS_SELECTOR,
                "input[name='email']",
            )
        )
    )
    email_.send_keys(email)

    password_ = browser.find_element_by_css_selector("input[name='password']")
    password_.send_keys(password)
    password_.send_keys(Keys.RETURN)

    if not path.parent.exists():
        path.parent.mkdir(parents=True)
    with open(path, "w") as f:
        f.write(f"{email}\n")
        f.write(f"{password}\n")

    return browser


##########
# search #
##########


def search(browser: WebDriver) -> dict[str, str]:
    WebDriverWait(browser, timeout).until(
        presence_of_element_located(
            (
                By.CSS_SELECTOR,
                "a[href='/search']",
            )
        )
    ).click()

    print("".ljust(80), end="\r")
    search = input("Search: ")

    search_bar = WebDriverWait(browser, timeout).until(
        presence_of_element_located(
            (
                By.TAG_NAME,
                "input",
            )
        )
    )
    search_bar.send_keys(search)

    results_ = WebDriverWait(browser, timeout).until(
        presence_of_all_elements_located(
            (
                By.CSS_SELECTOR,
                "button[data-testid='song-cover-has-access:true']",
            )
        )
    )

    results = [extract_song_info(element) for element in results_]
    print("Results:")
    for i, result in enumerate(results):
        print(f"[{i+1}] title: {result['title']}")
        print(f"{' ' * (len(str(i))+2)} artist: {result['artist']}")
        print(
            f"{' ' * (len(str(i))+2)} levels: "
            + ", ".join(
                f"({j+1}) {level}" for j, level in enumerate(result["levels"])
            )
        )
    selection = int(input("Which partition would you like to download? "))
    level = int(input("Which level would you like to download? "))

    results_[selection - 1].click()

    level_ = ["easy", "medium", "hard", "pro"].index(
        results[selection - 1]["levels"][level - 1]
    ) + 1

    WebDriverWait(browser, timeout).until(
        element_to_be_clickable(
            (
                By.CSS_SELECTOR,
                f"button[data-testid='level-selector-button:level-{level_}']",
            )
        )
    ).click()

    WebDriverWait(browser, timeout).until(
        presence_of_element_located(
            (
                By.CSS_SELECTOR,
                "button[data-testid='song-group-modal:open-song-button']",
            )
        )
    ).click()

    return {
        "title": results[selection - 1]["title"],  # type: ignore
        "artist": results[selection - 1]["artist"],  # type: ignore
        "level": results[selection - 1]["levels"][level - 1],  # type: ignore
    }


def extract_song_info(
    element: FirefoxWebElement,
) -> dict[str, str | list[str]]:
    divs = element.find_elements_by_tag_name("div")
    title = divs[-1].text
    artist = element.find_element_by_xpath("./p").text
    levels = [
        convert_level(div.value_of_css_property("background-color"))
        for div in divs[:-1]
    ]
    print(artist)
    return {"title": title, "artist": artist, "levels": levels}


def convert_level(level: str) -> str:
    return {
        "rgb(86, 233, 7)": "easy",
        "rgb(255, 208, 0)": "medium",
        "rgb(219, 10, 20)": "hard",
        "rgb(162, 86, 248)": "pro",
    }[level]


############
# download #
############


def download(browser: WebDriver, info: dict[str, Any]) -> dict[str, Any]:
    dir_ = (
        Path("/tmp")
        / f"{info['artist']}_{info['title']}_{info['level']}".replace(" ", "_")
        .replace("_-", "")
        .lower()
    )
    if not dir_.exists():
        dir_.mkdir(parents=True)

    WebDriverWait(browser, timeout).until(
        presence_of_element_located(
            (By.XPATH, "//button[normalize-space()='Skip']")
        )
    ).click()

    imgs = [
        img.get_attribute("src")
        for img in WebDriverWait(browser, timeout).until(
            presence_of_all_elements_located((By.CLASS_NAME, "split-image"))
        )
    ]

    paths = [dir_ / img.split("/")[-1].strip() for img in imgs]

    for i, (img, path) in enumerate(zip(imgs, paths)):
        load_print("Downloading images", f"{i+1}/{len(imgs)}")
        with open(path, "wb") as f:
            resp = httpx.get(img)
            f.write(resp.content)

    done_print("Downloaded images")

    return {**info, "images": list(set(paths))}


################
# pdf creation #
################


def create_pdf(info: dict[str, Any]) -> None:
    load_print("Creating pdf")
    filename = (
        f"{info['artist']}_{info['title']}_{info['level']}.pdf".replace(
            " ", "_"
        )
        .replace("_-", "")
        .lower()
    )

    pdf = canvas.Canvas(filename)
    pdf.setTitle(f"{info['title']} by {info['artist']} - {info['level']}")

    pdf.setFontSize(24)
    pdf.drawCentredString(300, 770, info["title"])
    pdf.setFontSize(12)
    pdf.drawCentredString(300, 750, "by")
    pdf.setFontSize(16)
    pdf.drawCentredString(300, 730, info["artist"])
    pdf.setFontSize(14)
    pdf.drawCentredString(300, 710, f"Level: {info['level']}")

    images = sorted(info["images"], key=lambda x: int(x.stem))

    widths: list[float] = []
    heights: list[float] = []
    for image in images:
        image_ = Image.open(image)
        widths.append(image_.size[0])
        heights.append(image_.size[1])
    height = max(heights) / max(widths) * 250
    widths = [w / max(widths) * 250 for w in widths]

    padding = 50.0

    current_x = padding
    current_y = 700.0 - height
    for i in range(len(images)):
        if widths[i] < padding:
            break
        if current_x + widths[i] > 600 - padding:
            current_x = padding
            current_y = current_y - height - 10
        if current_y < padding:
            pdf.showPage()
            current_y = 750 - padding
        pdf.drawImage(
            ImageReader(trim_border(Image.open(images[i]))),
            current_x,
            current_y,
            width=widths[i],
            height=height,
            mask="auto",
        )
        current_x = current_x + widths[i]

    pdf.save()

    done_print("Created pdf")


def trim_border(image: Image) -> Image:
    bg = Image.new(image.mode, image.size, image.getpixel((0, 0)))
    diff = ImageChops.difference(image, bg)
    diff = ImageChops.add(diff, diff, 2.0, -100)
    bbox = diff.getbbox()
    if bbox:
        image = image.crop((bbox[0], 0, bbox[2], image.size[1]))

    image = image.convert("RGBA")
    data = image.getdata()
    new_data = []
    for x in data:
        if x[3] == 0:
            new_data.append((255, 255, 255, 255))
        else:
            new_data.append(x)
    image.putdata(new_data)

    return image


#########
# utils #
#########


def load_print(text: str, symbol_content: str = "*", indent: int = 0) -> None:
    symbol = f"\033[1m[{symbol_content}]\033[0m"
    print(
        f"{' ' * indent}{symbol} {text}".ljust(
            shutil.get_terminal_size().columns
        ),
        end="\r",
    )


def done_print(text: str, symbol_content: str = "+", indent: int = 0) -> None:
    symbol = f"\033[1m\033[92m[{symbol_content}]\033[0m"
    print(
        f"{' ' * indent}{symbol} {text}".ljust(
            shutil.get_terminal_size().columns
        )
    )


if __name__ == "__main__":
    main()
